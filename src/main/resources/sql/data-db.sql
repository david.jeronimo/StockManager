INSERT INTO product (product_id, product_code, product_price) VALUES
(100, 'TS2017001', 12.45),
(101, 'TS2017002', 12.45);

INSERT INTO product_refill (refill_product_id, refill_quantity, refill_created_date) VALUES
(100, 200, CURRENT_TIMESTAMP),
(100, 50, CURRENT_TIMESTAMP),
(101, 50, CURRENT_TIMESTAMP);
