package david.stockmanager.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import david.stockmanager.dto.ProductDTO;
import david.stockmanager.dto.ProductSaleDTO;
import david.stockmanager.dto.ProductSalesSummaryDTO;

@Service
public class StockRepository {
  @Autowired
  private ProductDAO productDao;

  public long retrieveNumberOfAvailableItems(final String productCode) {
    return productDao.retrieveNumberOfAvailableItems(productCode);
  }

  public List<ProductSalesSummaryDTO> retrieveAllProductSalesSummaries() {
    return productDao.retrieveAllProductSalesSummaries();
  }

  public void createSale(final ProductSaleDTO sale) {
    productDao.persistSale(sale);
  }

  public ProductDTO retrieveProduct(String productCode) {
    return productDao.retrieveProduct(productCode);
  }
}
