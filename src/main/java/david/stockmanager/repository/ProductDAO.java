package david.stockmanager.repository;

import java.util.List;

import david.stockmanager.dto.ProductDTO;
import david.stockmanager.dto.ProductRefillDTO;
import david.stockmanager.dto.ProductSaleDTO;
import david.stockmanager.dto.ProductSalesSummaryDTO;

public interface ProductDAO {

  void persistProduct(ProductDTO product);

  void persistProductRefill(ProductRefillDTO refill);

  long retrieveNumberOfAvailableItems(String productCode);

  List<ProductSalesSummaryDTO> retrieveAllProductSalesSummaries();

  void persistSale(ProductSaleDTO sale);

  ProductDTO retrieveProduct(String productCode);

}
