package david.stockmanager.repository;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import david.stockmanager.common.hibernate.AbstractHibernateDAO;
import david.stockmanager.dto.ProductDTO;
import david.stockmanager.dto.ProductRefillDTO;
import david.stockmanager.dto.ProductSaleDTO;
import david.stockmanager.dto.ProductSalesSummaryDTO;

@Repository
public class ProductDAOImpl extends AbstractHibernateDAO implements ProductDAO {

  @Autowired
  public ProductDAOImpl(SessionFactory sessionFactory) {
    super(ProductDTO.class, sessionFactory);
  }

  @Override
  public ProductDTO retrieveProduct(String productCode) {
    Criteria criteria = criteria();
    criteria.add(Restrictions.eq("code", productCode));
    return (ProductDTO) criteria.uniqueResult();
  }

  @Override
  @Transactional(propagation = Propagation.MANDATORY)
  public void persistProduct(ProductDTO product) {
    currentSession().save(product);

  }

  @Override
  @Transactional(propagation = Propagation.MANDATORY)
  public void persistProductRefill(ProductRefillDTO refill) {
    currentSession().save(refill);
  }


  @Override
  @Transactional(propagation = Propagation.SUPPORTS)
  public long retrieveNumberOfAvailableItems(final String productCode) {
    final ProductSalesSummaryDTO summary = retrieveProductSalesSummary(productCode);
    return summary.getNumRefills() - summary.getNumSales();
  }

  private ProductSalesSummaryDTO retrieveProductSalesSummary(final String productCode) {
    Criteria criteria = createProductSalesSummaryCriteria();
    criteria.add(Restrictions.eq("code", productCode));
    return (ProductSalesSummaryDTO) criteria.uniqueResult();
  }

  private Criteria createProductSalesSummaryCriteria() {
    Criteria criteria = criteria();
    criteria.createAlias("refills", "refill", JoinType.LEFT_OUTER_JOIN);
    criteria.createAlias("sales", "sale", JoinType.LEFT_OUTER_JOIN);
    ProjectionList projectionList = Projections.projectionList();
    projectionList.add(Projections.alias(Projections.groupProperty("code"), "code"));
    projectionList.add(Projections.alias(Projections.groupProperty("price"), "price"));
    projectionList.add(Projections.alias(Projections.sum("refill.quantity"), "numRefills"));
    projectionList.add(Projections.alias(Projections.sum("sale.quantity"), "numSales"));
    criteria.setProjection(projectionList);
    criteria.setResultTransformer(Transformers.aliasToBean(ProductSalesSummaryDTO.class));
    return criteria;
  }

  @Override
  @Transactional(propagation = Propagation.SUPPORTS)
  public List<ProductSalesSummaryDTO> retrieveAllProductSalesSummaries() {
    Criteria criteria = createProductSalesSummaryCriteria();

    @SuppressWarnings("unchecked")
    final List<ProductSalesSummaryDTO> result = criteria.list();
    return result;
  }

  @Override
  @Transactional(propagation = Propagation.MANDATORY)
  public void persistSale(ProductSaleDTO sale) {
    currentSession().save(sale);
  }

}
