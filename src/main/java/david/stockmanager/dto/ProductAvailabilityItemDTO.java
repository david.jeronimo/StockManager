package david.stockmanager.dto;

import java.math.BigDecimal;

public class ProductAvailabilityItemDTO {
  private String productCode;
  private BigDecimal price;
  private long availableItems;

  public String getProductCode() {
    return productCode;
  }

  public void setProductCode(String productCode) {
    this.productCode = productCode;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public long getAvailableItems() {
    return availableItems;
  }

  public void setAvailableItems(long availableItems) {
    this.availableItems = availableItems;
  }


}
