package david.stockmanager.dto;

import java.math.BigDecimal;

public class ProductSalesSummaryDTO {
  private String code;
  private BigDecimal price;
  private long numRefills;
  private long numSales;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public long getNumRefills() {
    return numRefills;
  }

  public void setNumRefills(Long numRefills) {
    numRefills = (numRefills == null) ? 0 : numRefills;
    this.numRefills = numRefills;
  }

  public long getNumSales() {
    return numSales;
  }

  public void setNumSales(Long numSales) {
    numSales = (numSales == null) ? 0 : numSales;
    this.numSales = numSales;
  }


}
