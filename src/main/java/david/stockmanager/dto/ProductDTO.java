package david.stockmanager.dto;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class ProductDTO {
  private long id;
  private String code;
  private BigDecimal price;
  private List<ProductRefillDTO> refills;
  private List<ProductSaleDTO> sales;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "product_id")
  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  @Column(name = "product_code")
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  @Column(name = "product_price")
  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
  public List<ProductRefillDTO> getRefills() {
    return refills;
  }

  public void setRefills(List<ProductRefillDTO> refills) {
    this.refills = refills;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "product")
  public List<ProductSaleDTO> getSales() {
    return sales;
  }

  public void setSales(List<ProductSaleDTO> sales) {
    this.sales = sales;
  }



}
