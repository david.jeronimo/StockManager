package david.stockmanager.domain;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotEmpty;

public class ProductSaleRequest {
  private long userId;
  private String productCode;
  private int quantity;

  @Min(value = 1)
  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  @NotEmpty
  public String getProductCode() {
    return productCode;
  }

  public void setProductCode(String productCode) {
    this.productCode = productCode;
  }

  @Min(value = 1)
  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }


}
