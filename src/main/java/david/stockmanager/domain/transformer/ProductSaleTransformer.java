package david.stockmanager.domain.transformer;

import david.stockmanager.domain.ProductSaleRequest;
import david.stockmanager.dto.ProductSaleDTO;

public class ProductSaleTransformer {

  public static ProductSaleDTO transform(ProductSaleRequest saleRequest) {
    ProductSaleDTO productSale = new ProductSaleDTO();
    productSale.setQuantity(saleRequest.getQuantity());
    productSale.setUserId(saleRequest.getUserId());
    return productSale;
  }
}
