package david.stockmanager.common.hibernate;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class AbstractHibernateDAO {
  protected Class entityClass;
  protected SessionFactory sessionFactory;

  public AbstractHibernateDAO(Class entityClass, SessionFactory sessionFactory) {
    this.entityClass = entityClass;
    this.sessionFactory = sessionFactory;
  }

  protected Session currentSession() {
    return sessionFactory.getCurrentSession();
  }

  protected Criteria criteria() {
    return currentSession().createCriteria(entityClass);
  }
}
