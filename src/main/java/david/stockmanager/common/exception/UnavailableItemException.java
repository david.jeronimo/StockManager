package david.stockmanager.common.exception;

@SuppressWarnings("serial")
public class UnavailableItemException extends RuntimeException {
  private long numAvailableItems;
  private int numPurchasedItems;
  private String errorMessage;

  public UnavailableItemException(long numAvailableItems, int numPurchasedItems) {
    this.numAvailableItems = numAvailableItems;
    this.numPurchasedItems = numPurchasedItems;
  }

  public long getNumAvailableItems() {
    return numAvailableItems;
  }

  public int getNumPurchasedItems() {
    return numPurchasedItems;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }



}
