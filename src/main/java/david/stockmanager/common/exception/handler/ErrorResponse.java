package david.stockmanager.common.exception.handler;

public class ErrorResponse {
  private String errorMessage;

  public ErrorResponse(final String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public String getErrorMessage() {
    return errorMessage;
  }



}
