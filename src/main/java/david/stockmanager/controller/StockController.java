package david.stockmanager.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import david.stockmanager.domain.ProductSaleRequest;
import david.stockmanager.dto.ProductAvailabilityItemDTO;
import david.stockmanager.facade.StockFacade;

@Controller
@RequestMapping("/stock")
public class StockController {
  @Autowired
  private StockFacade stockFacade;

  @RequestMapping(value = "/products", method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public @ResponseBody
  List<ProductAvailabilityItemDTO> retrieveAllProductsWithAvailability() {
    return stockFacade.retrieveAllProductsWithAvailability();
  }

  @RequestMapping(value = "/products/buy", method = RequestMethod.POST)
  @ResponseStatus(HttpStatus.ACCEPTED)
  public void buyProduct(@Valid @RequestBody final ProductSaleRequest saleRequest) {
    stockFacade.buyProduct(saleRequest);
  }
}
