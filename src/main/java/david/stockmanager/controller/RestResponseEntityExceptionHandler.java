package david.stockmanager.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import david.stockmanager.common.exception.UnavailableItemException;
import david.stockmanager.common.exception.handler.ErrorResponse;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(value = {UnavailableItemException.class})
  @ResponseStatus(HttpStatus.GONE)
  protected @ResponseBody
  ErrorResponse handleUnavailableItemException(UnavailableItemException ex, WebRequest request) {
    return new ErrorResponse("The purchase could not be completed. Available items: "
        + ex.getNumAvailableItems() + ". Purchased items: " + ex.getNumPurchasedItems());
  }
}
