package david.stockmanager.service;

import java.util.List;

import david.stockmanager.domain.ProductSaleRequest;
import david.stockmanager.dto.ProductAvailabilityItemDTO;

public interface StockService {

  List<ProductAvailabilityItemDTO> retrieveAllProductsWithAvailability();

  void buyProduct(ProductSaleRequest saleRequest);

}
