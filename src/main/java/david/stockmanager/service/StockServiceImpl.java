package david.stockmanager.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import david.stockmanager.common.exception.UnavailableItemException;
import david.stockmanager.domain.ProductSaleRequest;
import david.stockmanager.domain.transformer.ProductSaleTransformer;
import david.stockmanager.dto.ProductAvailabilityItemDTO;
import david.stockmanager.dto.ProductDTO;
import david.stockmanager.dto.ProductSaleDTO;
import david.stockmanager.dto.ProductSalesSummaryDTO;
import david.stockmanager.repository.StockRepository;

@Service
public class StockServiceImpl implements StockService {
  @Autowired
  private StockRepository stockRepository;

  @Override
  public List<ProductAvailabilityItemDTO> retrieveAllProductsWithAvailability() {
    List<ProductSalesSummaryDTO> products = stockRepository.retrieveAllProductSalesSummaries();
    List<ProductAvailabilityItemDTO> results = new ArrayList<ProductAvailabilityItemDTO>();
    for (ProductSalesSummaryDTO product : products) {
      final ProductAvailabilityItemDTO resultItem = new ProductAvailabilityItemDTO();
      BigDecimal price = product.getPrice();
      resultItem.setPrice(price.setScale(2));
      resultItem.setProductCode(product.getCode());
      resultItem.setAvailableItems(product.getNumRefills() - product.getNumSales());
      results.add(resultItem);
    }
    return results;
  }

  @Override
  public void buyProduct(final ProductSaleRequest saleRequest) {
    final long numAvailableItems =
        stockRepository.retrieveNumberOfAvailableItems(saleRequest.getProductCode());
    if (numAvailableItems < saleRequest.getQuantity()) {
      throw new UnavailableItemException(numAvailableItems, saleRequest.getQuantity());
    }
    final ProductDTO product = stockRepository.retrieveProduct(saleRequest.getProductCode());
    final ProductSaleDTO sale = ProductSaleTransformer.transform(saleRequest);
    sale.setCreatedDate(new Date());
    sale.setProduct(product);
    stockRepository.createSale(sale);

  }
}
