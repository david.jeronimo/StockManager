package david.stockmanager.facade;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import david.stockmanager.domain.ProductSaleRequest;
import david.stockmanager.dto.ProductAvailabilityItemDTO;
import david.stockmanager.service.StockService;

@Service
public class StockFacadeImpl implements StockFacade {
  @Autowired
  private StockService stockService;

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public List<ProductAvailabilityItemDTO> retrieveAllProductsWithAvailability() {
    return stockService.retrieveAllProductsWithAvailability();
  }

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void buyProduct(final ProductSaleRequest saleRequest) {
    stockService.buyProduct(saleRequest);
  }
}
