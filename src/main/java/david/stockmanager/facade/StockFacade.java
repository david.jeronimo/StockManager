package david.stockmanager.facade;

import java.util.List;

import david.stockmanager.domain.ProductSaleRequest;
import david.stockmanager.dto.ProductAvailabilityItemDTO;

public interface StockFacade {

  List<ProductAvailabilityItemDTO> retrieveAllProductsWithAvailability();

  void buyProduct(ProductSaleRequest saleRequest);

}
