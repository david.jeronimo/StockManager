package david.stockmanager.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import david.stockmanager.common.exception.UnavailableItemException;
import david.stockmanager.domain.ProductSaleRequest;
import david.stockmanager.domain.ProductSaleRequestBuilder;
import david.stockmanager.dto.ProductDTO;
import david.stockmanager.dto.ProductDTOBuilder;
import david.stockmanager.dto.ProductSaleDTO;
import david.stockmanager.repository.StockRepository;

public class StockServiceTest {
  @InjectMocks
  private StockServiceImpl stockService;

  @Mock
  private StockRepository stockRepository;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
  }


  @Test(expected = UnavailableItemException.class)
  public void testBuyProduct_threeBought_noneAvailable() {
    String productCode = "code";
    int quantity = 3;
    ProductSaleRequest saleRequest =
        (new ProductSaleRequestBuilder()).defaultValues().withProductCode(productCode)
            .withQuantity(quantity).build();
    when(stockRepository.retrieveNumberOfAvailableItems(productCode)).thenReturn((long) 0);

    stockService.buyProduct(saleRequest);
  }

  @Test
  public void testBuyProduct_threeBought_fourAvailable() {
    long userId = 123;
    String productCode = "code";
    int quantity = 3;
    ProductSaleRequest saleRequest =
        (new ProductSaleRequestBuilder()).withProductCode(productCode).withQuantity(quantity)
            .withUserId(userId).build();
    when(stockRepository.retrieveNumberOfAvailableItems(productCode)).thenReturn((long) 4);
    ProductDTO product = (new ProductDTOBuilder()).defaultValues().withCode(productCode).build();
    when(stockRepository.retrieveProduct(productCode)).thenReturn(product);
    ArgumentCaptor<ProductSaleDTO> saleCaptor = ArgumentCaptor.forClass(ProductSaleDTO.class);

    stockService.buyProduct(saleRequest);

    verify(stockRepository, times(1)).createSale(saleCaptor.capture());
    ProductSaleDTO sale = saleCaptor.getValue();
    assertEquals(userId, sale.getUserId());
    assertEquals(quantity, sale.getQuantity());
    assertEquals(productCode, sale.getProduct().getCode());
    assertNotNull(sale.getCreatedDate());
  }

  @Test
  public void testBuyProduct_threeBought_threeAvailable() {
    long userId = 123;
    String productCode = "code";
    int quantity = 3;
    ProductSaleRequest saleRequest =
        (new ProductSaleRequestBuilder()).withProductCode(productCode).withQuantity(quantity)
            .withUserId(userId).build();
    when(stockRepository.retrieveNumberOfAvailableItems(productCode)).thenReturn((long) 3);
    ProductDTO product = (new ProductDTOBuilder()).defaultValues().withCode(productCode).build();
    when(stockRepository.retrieveProduct(productCode)).thenReturn(product);
    ArgumentCaptor<ProductSaleDTO> saleCaptor = ArgumentCaptor.forClass(ProductSaleDTO.class);

    stockService.buyProduct(saleRequest);

    verify(stockRepository, times(1)).createSale(saleCaptor.capture());
    ProductSaleDTO sale = saleCaptor.getValue();
    assertEquals(userId, sale.getUserId());
    assertEquals(quantity, sale.getQuantity());
    assertEquals(productCode, sale.getProduct().getCode());
    assertNotNull(sale.getCreatedDate());
  }

}
