package david.stockmanager.domain;

public class ProductSaleRequestBuilder {
  private ProductSaleRequest saleRequest;

  public ProductSaleRequestBuilder() {
    saleRequest = new ProductSaleRequest();
  }

  public ProductSaleRequest build() {
    return saleRequest;
  }

  public ProductSaleRequestBuilder defaultValues() {
    saleRequest.setProductCode("product code");
    saleRequest.setQuantity(1);
    saleRequest.setUserId(123);
    return this;
  }

  public ProductSaleRequestBuilder withProductCode(String productCode) {
    saleRequest.setProductCode(productCode);
    return this;
  }

  public ProductSaleRequestBuilder withQuantity(int quantity) {
    saleRequest.setQuantity(quantity);
    return this;
  }

  public ProductSaleRequestBuilder withUserId(long userId) {
    saleRequest.setUserId(userId);
    return this;
  }
}
