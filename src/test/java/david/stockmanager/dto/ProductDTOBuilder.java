package david.stockmanager.dto;

import java.math.BigDecimal;

public class ProductDTOBuilder {
  private ProductDTO productDTO;

  public ProductDTOBuilder() {
    productDTO = new ProductDTO();
  }

  public ProductDTO build() {
    return productDTO;
  }

  public ProductDTOBuilder defaultValues() {
    productDTO.setCode("product code");
    productDTO.setPrice(new BigDecimal(12.3));
    return this;
  }

  public ProductDTOBuilder withCode(String code) {
    productDTO.setCode(code);
    return this;
  }
}
