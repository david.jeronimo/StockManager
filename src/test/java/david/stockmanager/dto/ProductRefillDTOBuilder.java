package david.stockmanager.dto;

import java.util.Date;

public class ProductRefillDTOBuilder {
  private ProductRefillDTO refill;

  public ProductRefillDTOBuilder() {
    refill = new ProductRefillDTO();
  }

  public ProductRefillDTO build() {
    return refill;
  }

  public ProductRefillDTOBuilder defaultValues() {
    refill.setQuantity(1);
    refill.setCreatedDate(new Date());
    return this;
  }

  public ProductRefillDTOBuilder withQuantity(int quantity) {
    refill.setQuantity(quantity);
    return this;
  }

  public ProductRefillDTOBuilder withProduct(ProductDTO product) {
    refill.setProduct(product);
    return this;
  }
}
