package david.stockmanager.dto;

import java.util.Date;

public class ProductSaleDTOBuilder {
  private ProductSaleDTO sale;

  public ProductSaleDTOBuilder() {
    sale = new ProductSaleDTO();
  }

  public ProductSaleDTO build() {
    return sale;
  }

  public ProductSaleDTOBuilder defaultValues() {
    sale.setUserId(123);
    sale.setQuantity(1);
    sale.setCreatedDate(new Date());
    return this;
  }

  public ProductSaleDTOBuilder withProduct(ProductDTO product) {
    sale.setProduct(product);
    return this;
  }
}
