package david.stockmanager.repository;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import david.stockmanager.dto.ProductDTO;
import david.stockmanager.dto.ProductDTOBuilder;
import david.stockmanager.dto.ProductRefillDTO;
import david.stockmanager.dto.ProductRefillDTOBuilder;
import david.stockmanager.dto.ProductSaleDTO;
import david.stockmanager.dto.ProductSaleDTOBuilder;
import david.util.IntegrationTest;

public class ProductDAOTest extends IntegrationTest {
  @Autowired
  private ProductDAO productDAO;

  @Test
  public void checkAvailableItems_oneProductNoRefill() {
    final String productCode = "Product Code";
    final ProductDTO product =
        (new ProductDTOBuilder()).defaultValues().withCode(productCode).build();
    productDAO.persistProduct(product);
    assertEquals(0, productDAO.retrieveNumberOfAvailableItems(productCode));
  }

  @Test
  public void checkAvailableItems_oneProductOneRefillTwoAvailable() {
    final String productCode = "Product Code";
    final ProductDTO product =
        (new ProductDTOBuilder()).defaultValues().withCode(productCode).build();
    productDAO.persistProduct(product);
    final ProductRefillDTO refill =
        (new ProductRefillDTOBuilder()).defaultValues().withProduct(product).withQuantity(2)
            .build();
    productDAO.persistProductRefill(refill);

    assertEquals(2, productDAO.retrieveNumberOfAvailableItems(productCode));
  }

  @Test
  public void createSale() {
    final String productCode = "Product Code";
    final ProductDTO product =
        (new ProductDTOBuilder()).defaultValues().withCode(productCode).build();
    productDAO.persistProduct(product);
    final ProductRefillDTO refill =
        (new ProductRefillDTOBuilder()).defaultValues().withProduct(product).withQuantity(2)
            .build();
    productDAO.persistProductRefill(refill);
    final ProductSaleDTO sale =
        (new ProductSaleDTOBuilder()).defaultValues().withProduct(product).build();
    productDAO.persistSale(sale);

    assertEquals(1, productDAO.retrieveNumberOfAvailableItems(productCode));
  }
}
