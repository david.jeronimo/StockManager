### Setup ###

In order to run the application, the following steps are required:
mvn clean install
mvn jetty:run

Jetty will run on port 8080, so the URLs will be in the form localhost:8080/stockmanager/products

Maven 3 and Java 7(+) are required.

An embedded database will be created and populated with a couple of products when Jetty starts.

### Usage ###
Two URLs have been exposed:

GET http://localhost:8080/stock/products
Sample response:
[
    {
        "productCode": "TS2017001",
        "price": 12.45,
        "availableItems": 250
    },
    {
        "productCode": "TS2017002",
        "price": 12.45,
        "availableItems": 47
    }
]

POST http://localhost:8080/stock/products/buy
Sample request:
{
    "userId": 123,
    "productCode": "TS2017002",
    "quantity": 3
}
